from api.v1.middlewares import *


async def create_task(request):
    try:
        await authorize_request(request)

        data = await request.json()

        query = ("INSERT INTO tokens(user, name_task, description, time_of_creation, status, data_of_completion) "
                 "VALUES(%s, %s, %s, "
                 "%s, %s)",
                 (data['username'], data['name_task'], data['description'], time.time(),
                  data['status'], data['data_of_completion']))

        client = request.app[APP_DB_MANAGER]

        await client.fetch_all(query)

    except Exception as e:
        logging.error(str(sys.exc_info()))
        logging.error(str(e))


async def get_tasks(request):
    try:
        await authorize_request(request)

        data = await request.json()

        query = ("SELECT name_task, description, time_of_creation, status, data_of_completion FROM tasks WHERE user = "
                 "'%s' and status = "
                 "'%s' and data_of_completion = "
                 "'%s' ",
                 (data['username'], data['status'], data['data_of_completion']))

        client = request.app[APP_DB_MANAGER]

        records = await client.fetch_all(query)

        tasks = []

        for val in records:
            dic = {
                "user": data['username'],
                "name_task": val[0],
                "description": val[1],
                "time_of_creation": val[2],
                "status": data['status'],
                "data_of_completion": data['data_of_completion'],
            }

            tasks.append(dic)

        res = {
            "count": len(records),
            "tasks": tasks
        }

        return web.json_response(res)

    except Exception as e:
        logging.error(str(sys.exc_info()))
        logging.error(str(e))


async def update_status(request):
    try:
        await authorize_request(request)

        data = await request.json()

        query = (""" UPDATE tasks
                SET status = %s
                WHERE user = %s""",
                 (data['status'], data['username']))

        client = request.app[APP_DB_MANAGER]

        await client.fetch_all(query)

    except Exception as e:
        logging.error(str(sys.exc_info()))
        logging.error(str(e))


async def update_other(request):
    try:
        await authorize_request(request)

        data = await request.json()

        query = (""" UPDATE tasks
                SET data_of_completion = %s, name_task = %s, description = %s
                WHERE user = %s""",
                 (data['data_of_completion'], data['name_task'],
                  data['description'], data['user']))

        client = request.app[APP_DB_MANAGER]

        await client.fetch_all(query)

    except Exception as e:
        logging.error(str(sys.exc_info()))
        logging.error(str(e))
