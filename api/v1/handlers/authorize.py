"""
Handlers
"""
import aiohttp

from api.v1.middlewares import *

shared_key_private = """BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC0+AOxorMabKQT
LbWtQ3elPIXMxGy6yE8+pbIbfM+CCGBZRcaoMjVPf1232gMqGomm1XMKTm+XCAg4
s3vLDQvlBgd9H66K8dHqIoSLMbDLPrEY3s8H35+RkwJ0ZwKLJZAQMs28dNENRLYG
JYsbOydo9TK8hMr47MuuXU6uyB1NPRmyNGqSbksoTLUBgLEEGhfl7MSbkRHbsS3c
knaUvPJJe8M6ePlJsmItzo02PFcv3Zi+KPIsIacO5fQ7WWfX4FwfahArdqsFtVRk
yGgLfBe0rjIR1yupqnXo4amdbq1hg5zk7+vsy9PIqc8otCccCoyjkP31YqLjOIdQ
5q0OXcJ5AgMBAAECggEBAKQKBN8qSgtz2y5/SqWp+HRRbj909dJB7IMvE0mTU03d
ZejGNRMVn/lVzI/xENlDgO83oLYA72oKH9m16Ergop2RX4xuAXt1RABmFZtPCaDS
TRnsLeJHNI9yoND+5vFWtUXZp2Biy3Spig38TiH69vhO+xSjkpEdpGuDrOUGBHmT
mINYvePfx+JvVjXuH5Kt57QlAYNjfqkd3hv0AEmfioLi3vLuCZH/ZIgH8bjm2zIm
UO3sSaTyZ8edt3uESbPeKfIViAv/X7HDiWXL1/BD9dHEOhlGgqqxR8LgGPSq8CcA
lpFUkONVm+kuuLUWcMmm4aBIImzOSDOJGZfI/dvW0vkCgYEA4DY/Su/5tIGALq4N
MadxsBJB1VW4b4t0tJI/3VveB50KZVHF12TGUwX2iz817otBiF5/5q7RECe38/Nw
KGlRsaY53OLFx5nVrIrB0rb1FLPK2o0Z9WdB/NGh2EZhwhvFMzDucb3Tr3fng4s1
p2IVNItjfONIwEb4IFU2Oa4/WyMCgYEAzqBDNAp5tYFjgOqnzDPTK3G9TXBHuD7K
L2LXniu+y1w0rv6CYhPE2/jPSWJVXSZmbOcN4F1zj5O8a3CioT6NS92FNKFnwqgu
hR8QfvSnZEQl2kY5sQS1FRIUJdEbvVXSscgFo59WVG4V5ZaewB/muuBhr+aDdZxo
40t8pT1L47MCgYBFfmrltjXGTfFGBqqOPmENessu7W/KGwZkRYRH/wZ1g6ZKOytA
FLsn1oxRIpl7w2vfcbLg9ERWZoksssKLtAEfN+9tWFiHAlZpMWTcA9s4vHVegieO
mZ+pVex9FWuNd4AK2tq9JGItW9PFQOuzYsJB1AZVGjldZ2HW27J/9FMs0wKBgQCP
S6GPk7w4XhUtsI7SFRbbyDNqYvW6FwGMv1WdQeU9jyKSQbsWXXNmwjSvRdzBjwH1
gH+lG+oqpmBDeoeun3zKIkRg7ZbR7mTkuVhtUypLZN+DcrRLyWHzzOfgGhbMxZoq
5PbUNIpgX0AcSsqpnvYan8XfgVOl2uT6zL0Ss2HP9QKBgQCKwG/thDUzu3H50tEg
nTgytbWtu0GsCF+/aOCIAO8gt0lrcr9hi2ZIy1INJkGuwLm/sFxRCMFoXYVQ8odA
kJ6IDuFT1jpKQaf4hIMFV3QtoTuEh0PPJUAku9BnXZVubEloaxTrGBYX6qWpZ8oY
C0tmdDp9di2gPp9rufOKLUMiAA==
-----END PRIVATE KEY-----
"""

shared_key_public = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsuSfXS5pRgzQRDf/3oyu
H2jdpM7aVgTBZxRXlZdt5A5Ossxeg7oI/Vsv2Cb+2cTR7GD+O7mbKceYgxUejIAU
YBwyIZt7Y5bKN49CB2mUBgkO74eZzfFR0rU6HWoK4WZOkvvfDXlGMwvQ972LTFoe
ly8yVHQ+r5Fkb/aJF9yNZOrnbuqA18kA+lEJYtGzt/N71roDfSgveKnxEv+2vOOc
C/TdK86NhSXzk/5LSjRRQyUxW9DGvdWx+zQ6HdqAwYL9eWmlQmT3ojq6+SbVI2Ux
M8pVeAARUdy06bLVS6HtfFTaeOpS+l0L7eWbmaziJsZCgsMdSIU/j4uwmgwNsatm
YwIDAQAB
-----END PUBLIC KEY-----"""


async def authorize(request):
    """
    Handler needed to get parameters for user authorization
    """
    try:

        token_endpoint = "http://localhost:8080/token"

        data = await request.json()

        if data["grant_type"] == "password":
            post_params = {"client_id": data["client_id"],
                           "client_secret": "abc",
                           "username": data["username"],
                           "grant_type": data["grant_type"],
                           "password": data["password"]}

            session = aiohttp.ClientSession()
            async with session.post(token_endpoint,
                                    data=post_params) as resp:
                resp = await resp.json()
            await session.close()

            query = ("INSERT INTO tokens(user, token) VALUES(%s, %s)",
                     (data['username'], resp['access_token']))

            client = request.app[APP_DB_MANAGER]

            await client.fetch_all(query)

            access_token = {
                "aud": data["client_id"],
                "jti": resp['access_token'],
                "exp": time.time() + resp['expires_in'],
                "username": data['username'],
                "scopes": [data['scope']]
            }

            refresh_token = {

                "aud": data["client_id"],
                "jti": resp['refresh_token'],
                "exp": time.time() + resp['expires_in'],
                "username": data['username'],
                "scopes": [data['scope']]
            }

            encoded_access = jwt.encode(access_token, shared_key_private, algorithm='HS256')
            encoded_refresh = jwt.encode(refresh_token, shared_key_private, algorithm='HS256')

            result = {
                "token_type": resp['token_type'],
                "expires_in": resp['expires_in'],
                "access_token": encoded_access.decode(),
                "refresh_token": encoded_refresh.decode(),
            }

            return web.json_response(result)

    except Exception as e:
        logging.error(str(sys.exc_info()))
        logging.error(str(e))


async def get_echo(request):
    """
    Handler needed to test_connection
    """
    await authorize_request(request)

    return web.json_response('Echo')
