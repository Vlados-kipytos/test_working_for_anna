"""
routes
"""

from api.v1.handlers.authorize import *
from api.v1.handlers.tasks import *


def setup_routes(app, provider, handler):
    app.router.add_post(provider.token_path, handler.post_dispatch_request)
    app.add_routes([
        web.post('/oauth/token', authorize),
        web.get('/echo', get_echo),
        web.post('/create_task', create_task),
        web.post('/get_task', get_tasks),
        web.patch('/update_status', update_status),
        web.patch('/update_other', update_status)])
