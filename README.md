Welcome to my project: task_manager 

Стек:

* из фреймворков был выбран Aiohttp 

* из баз данных был выбран Postgres

Для работы с моим приложением вам потребуется создать две таблицы :

1) table tokens где будут храниться токены зареганых пользователей:

c  полями:
* user (primary key)
* token 

2) table tasks для хранения ваших задач с полями:

* user (имя пользователя кому принадлежит задача)
* name ( primary key ) (название задачи)
* description (описание)
* status 
* time_of_creation (время создания задачи)
* data_of_completion (Планируемое дата завершения)


API:

 * /oauth/token:
 
 Служит для получения необходимого токена, чтобы в дальнейшем с помощью него 
 авторизоваться в api
 
 Recommended post request:
 
 {
    "grant_type":"password",
    "username": "admin",
    "password": "admin",
    "client_id": "mcu",
    "client_secret": "abc",
    "scope": "asas"
}

post reply:

{
    "token_type": "Bearer",
    "expires_in": 1000,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJtY3UiLCJqdGkiOiIxZGYyM2M4OS1jMGY5LTQ0MjgtYWNiMy03OTA4ZTEzZTQ4ZWYiLCJleHAiOjE2MDE1NzQ5NjYuOTUzNjYxLCJ1c2VybmFtZSI6ImFkbWluIiwic2NvcGVzIjpbImFzYXMiXX0.7fMzCHst-txZQWbQt_tuaqQXsfSDyy87Z0z7Zby72cw",
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJtY3UiLCJqdGkiOiI0NTVhOGZlMS1jMmY1LTQzNzEtOGNkZi05YWY5MjQwODY4YTUiLCJleHAiOjE2MDE1NzQ5NjYuOTUzNjY4LCJ1c2VybmFtZSI6ImFkbWluIiwic2NvcGVzIjpbImFzYXMiXX0.0xqCz-snw447rqfy6lAVsCh-nXQ6TVy0h2tW4BoNBgg"
}

После того как вы получили access_token, чтобы авторизоваться, вам 
необходимо создавать запросы в заголовке Authorization примера:

Bearer access_token 

Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJtY3UiLCJqdGkiOiIxZGYyM2M4OS1jMGY5LTQ0MjgtYWNiMy03OTA4ZTEzZTQ4ZWYiLCJleHAiOjE2MDE1NzQ5NjYuOTUzNjYxLCJ1c2VybmFtZSI6ImFkbWluIiwic2NvcGVzIjpbImFzYXMiXX0.7fMzCHst-txZQWbQt_tuaqQXsfSDyy87Z0z7Zby72cw
 
 * /echo
 
 Необходим для простого теста сервиса на работоспособность
 
 * /create_task
 
 Необходим для создания задачи пользователем 
 
 * /get_task
 
 Необходим для получения информации об задаче пользователя 
 
 * /update_status
 
 Необходим для изменения статуса пользователя
 
 * /update_other
 
 Необходим для изменения планируемое время завершения, название и описание


