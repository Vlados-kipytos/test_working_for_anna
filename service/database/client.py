import aiopg

from typing import Any, Optional, Tuple
from aiopg.pool import Pool


class Manager(object):
    _host: str = None
    _port: int = None
    _user: str = None
    _password: str = None
    _database: str = None
    _pool: Optional[Pool] = None

    def __init__(self,
                 host: str = None,
                 port: int = None,
                 user: str = None,
                 password: str = None,
                 database: str = None):
        self._host: str = host
        self._port: int = port
        self._user: str = user
        self._password: str = password
        self._database: str = database

    async def fetch_row(self, query: Any, parameters: Any = None, timeout: Optional[Any] = None) -> Tuple:
        await self._reconnect()

        connection: aiopg.Connection = await self._pool.acquire()
        cursor: aiopg.Cursor = await connection.cursor()
        await cursor.execute(query, parameters=parameters, timeout=timeout)
        result = await cursor.fetchone()
        await self._pool.release(connection)
        return result

    async def fetch_all(self, query: Any, parameters: Any = None, timeout: Optional[Any] = None) -> Tuple:
        await self._reconnect()

        connection: aiopg.Connection = await self._pool.acquire()
        cursor: aiopg.Cursor = await connection.cursor()
        await cursor.execute(query, parameters=parameters, timeout=timeout)
        result = await cursor.fetchall()
        await self._pool.release(connection)
        return result

    async def _reconnect(self):
        if self._pool is None or self._pool.closed is True:
            self._pool: Pool = await aiopg.create_pool(
                host=self._host,
                port=self._port,
                user=self._user,
                password=self._password,
                database=self._database,
                maxsize=5
            )

    async def close(self):
        if self._pool is None:
            return

        return self._pool.close()

    async def clear(self):
        if self._pool is None:
            return

        return self._pool.clear()
